import os

app_path = '/vissample'

dir_root = os.path.join(os.path.join(os.path.dirname(__file__), ".."), "..")

print dir_root
db = 'dantze.db'
img_path = os.path.join(dir_root, 'files')

img_tbl_name = 'images'
img_tbl_schema = {'img_title': 'text',
                  'img_description': 'text',
                  'img_src': 'text',
                  'img_data_tags': 'text',
                  'img_visual_tags': 'text',
                  'img_filename': 'text',
                  'img_timestamp': 'integer'}

usr_tbl_name = 'users'
usr_tbl_schema = {'usr_name': 'text'}


def create_table_cmd(tbl_name, tbl_schema):
    sql_str = 'create table ' + tbl_name + '('
    for k in tbl_schema.keys():
        sql_str += k + ' ' + tbl_schema[k] + ', '
    return sql_str[:-2] + ')'

create_img_tbl_str = create_table_cmd(img_tbl_name, img_tbl_schema)
create_usr_tbl_str = create_table_cmd(usr_tbl_name, usr_tbl_schema)




