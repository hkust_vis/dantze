from flask import render_template, request, url_for, send_from_directory, redirect
from dantze import app
from dantze import model
from dantze import config as cfg
from datetime import datetime
from werkzeug.utils import secure_filename
import os
import json


@app.route('/')
def index():
    return render_template('display.html')

@app.route('/book')
def book():
    return render_template('book.html')

@app.route('/static/<path:filename>')
def send(filename):
    print filename
    return send_from_directory('static/', filename)

@app.route('/files/<filename>')
def uploaded_file(filename):
    return send_from_directory(cfg.img_path, filename)

@app.route('/upload', methods=['POST', 'GET'])
def upload():
    if request.method == 'POST':
        img_url = ''
        file = request.files['file']
        if file is None:
            return json.dumps({'img_url': img_url})
        timestamp = int((datetime.now() - datetime.fromtimestamp(0)).total_seconds())
        filename = str(timestamp)[-7:-1] + secure_filename(file.filename)
        file.save(os.path.join(cfg.img_path, filename))
        img_url = 'files/' + filename
        img_data = request.form.to_dict(flat=True)
        img_data['img_filename'] = filename
        img_data['img_timestamp'] = timestamp
        model.update_image_table(img_data)
        return json.dumps({'img_url': img_url})
    else:
        return render_template('upload.html')

#TODO:update visualization description
@app.route('/update', methods=['POST'])
def update():
    return json.dumps()

#TODO:return visualization collections
@app.route('/collection', methods=['POST', 'GET'])
def get_collection():
    imgs = []
    attrs = ['img_filename', 'img_title', 'img_description', 'img_src']
    if request.method == 'GET':
        imgs = model.get_all(attrs)
    else:
        form_data = request.form.to_dict(flat=True)
        imgs = model.filter_by_tags(attrs, form_data['img_data_tags'],form_data['img_visual_tags'])
    for img in imgs:
        img['img_url'] = 'files/' + img['img_filename']
    return json.dumps(imgs)

@app.route('/display')
def display():
    return render_template('display.html')


@app.route('/autocomplete/<mode>')
def autocomplete(mode):
    tags = []
    if mode == 'data' or mode == 'visual':
        tags = model.get_tags(mode)
    return json.dumps(tags)






