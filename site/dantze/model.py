import sqlite3
import dantze.config as cfg


def init():
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    c.execute("select name from sqlite_master where type='table';")
    tbl_names = [r[0] for r in c.fetchall()]
    if cfg.img_tbl_name not in tbl_names:
        c.execute(cfg.create_img_tbl_str)
    if cfg.usr_tbl_name not in tbl_names:
        c.execute(cfg.create_usr_tbl_str)
    conn.commit()
    conn.close()


def update_image_table(data):
    print(data)
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    attrs = list(cfg.img_tbl_schema.keys())
    insert_str = "insert into " + cfg.img_tbl_name + "(" + ",".join(attrs) + ")" + "values (" + ",".join(['?'] * len(attrs)) + ")"
    c.execute(insert_str, [data[attr] for attr in attrs])
    conn.commit()
    conn.close()


def update_user_table(item):
    pass


#TODO: image retrieval (filter by tags)
def filter_by_tags(fields, data_tags, visual_tags):
    print('filter_by_tags')
    data_tags_set = set(str(data_tags).split(','))
    visual_tags_set = set(str(visual_tags).split(','))
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    attrs=",".join(fields + ["img_data_tags", "img_visual_tags"])
    c.execute("select " + attrs + " from " + cfg.img_tbl_name)
    all = c.fetchall()
    res = []
    for tpl in all:
        if (data_tags == '' or data_tags_set.issubset(set(str(tpl[len(fields)]).split(',')))) and \
            (visual_tags == '' or visual_tags_set.issubset(set(str(tpl[len(fields) + 1]).split(',')))):
            res.append(dict([(fields[i], tpl[i]) for i in range(0, len(fields))]))
    conn.commit()
    conn.close()
    return res


def get_all(fields):
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    attrs=",".join(fields)
    c.execute("select " + attrs + " from " + cfg.img_tbl_name)
    all = c.fetchall()
    res = []
    for tpl in all:
        res.append(dict([(fields[i], tpl[i]) for i in range(0, len(fields))]))
    conn.commit()
    conn.close()
    return res


def get_tags(mode):
    conn = sqlite3.connect(cfg.db)
    c = conn.cursor()
    if mode == 'data':
        c.execute('select ' + 'img_data_tags' + ' from ' + cfg.img_tbl_name)
    elif mode == 'visual':
        c.execute('select ' + 'img_visual_tags' + ' from ' + cfg.img_tbl_name)
    conn.commit()
    rs = c.fetchall()
    tags = set()
    for tpl in rs:
        tags.update(str(tpl[0]).split(','))
    tags = list(tags)
    conn.close()
    return tags

#TODO: user information retrieval








